# Defined via `source`
function yta-m4a --wraps='youtube-dl --add-metadata --embed-thumbnail --extract-audio --audio-format m4a' --description 'alias yta-m4a youtube-dl --add-metadata --embed-thumbnail --extract-audio --audio-format m4a'
  youtube-dl --add-metadata --embed-thumbnail --extract-audio --audio-format m4a $argv; 
end
