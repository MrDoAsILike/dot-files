# Defined via `source`
function rngr --wraps=ranger --description 'Ranger CLI file manager'
  ranger $argv; 
end
