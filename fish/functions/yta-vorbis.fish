# Defined via `source`
function yta-vorbis --wraps='youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail vorbis' --description 'alias yta-vorbis youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail vorbis'
  youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail vorbis $argv; 
end
