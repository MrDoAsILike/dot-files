# Defined via `source`
function yta-wav --wraps='youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail wav' --description 'alias yta-wav youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail wav'
  youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail wav $argv; 
end
