# Defined via `source`
function yta-opus --wraps='youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail opus' --description 'alias yta-opus youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail opus'
  youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail opus $argv; 
end
