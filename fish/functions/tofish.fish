# Defined via `source`
function tofish --wraps=sudo\ chsh\ orkah\ -s\ /bin/fish\ \&\&\ echo\ \'Now\ log\ out.\' --description alias\ tofish\ sudo\ chsh\ orkah\ -s\ /bin/fish\ \&\&\ echo\ \'Now\ log\ out.\'
  sudo chsh orkah -s /bin/fish && echo 'Now log out.' $argv; 
end
