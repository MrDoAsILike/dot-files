# Defined via `source`
function ytv-best --wraps='youtube-dl -f bestvideo+bestaudio --add-metadata --embed-thumbnail' --description 'alias ytv-best youtube-dl -f bestvideo+bestaudio --add-metadata --embed-thumbnail'
  youtube-dl -f bestvideo+bestaudio --add-metadata --embed-thumbnail $argv; 
end
