# Defined via `source`
function ll --wraps='exa long list with git status and colors'
  exa --icons --color-scale --color=auto --group-directories-first -F -H -ghla@ --git $argv; 
end
