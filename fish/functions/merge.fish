# Defined via `source`
function merge --wraps='xrbd -merge ~/.Xresources' --description 'alias merge xrbd -merge ~/.Xresources'
  xrbd -merge ~/.Xresources $argv; 
end
