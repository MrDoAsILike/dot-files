# Defined via `source`
function mmirrorlist --wraps='sudo micro /etc/pacman.d/mirrorlist' --description 'alias mmirrorlist sudo micro /etc/pacman.d/mirrorlist'
  sudo micro /etc/pacman.d/mirrorlist $argv; 
end
