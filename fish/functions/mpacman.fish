# Defined via `source`
function mpacman --wraps='sudo micro /etc/pacman.conf' --description 'alias mpacman sudo micro /etc/pacman.conf'
  sudo micro /etc/pacman.conf $argv; 
end
