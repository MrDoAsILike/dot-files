# Defined via `source`
function mirror --wraps=sudo\ reflector\ -p\ https,http,ftp\ -c\ \'US\'\ -f\ 25\ -l\ 50\ --number\ 20\ --sort\ rate\ --verbose\ --save\ /etc/pacman.d/mirrorlist --description alias\ mirror\ sudo\ reflector\ -p\ https,http,ftp\ -c\ \'US\'\ -f\ 25\ -l\ 50\ --number\ 20\ --sort\ rate\ --verbose\ --save\ /etc/pacman.d/mirrorlist
  sudo reflector -p https,http,ftp -c 'US' -f 25 -l 50 --number 20 --sort rate --verbose --save /etc/pacman.d/mirrorlist $argv; 
end
