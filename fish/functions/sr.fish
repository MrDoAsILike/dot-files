# Defined via `source`
function sr --wraps='sudo reboot' --description 'alias sr sudo reboot'
  sudo reboot $argv; 
end
