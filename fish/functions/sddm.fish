# Defined via `source`
function sddm --wraps='sudo micro /etc/sddm/sddm.conf' --description 'alias sddm sudo micro /etc/sddm/sddm.conf'
  sudo micro /etc/sddm/sddm.conf $argv; 
end
