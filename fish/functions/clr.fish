# Defined via `source`
function clr --wraps='clear && neofetch' --description 'clear the terminal and run neofetch'
  clear && neofetch $argv; 
end
