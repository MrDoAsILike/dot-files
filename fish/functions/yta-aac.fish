# Defined via `source`
function yta-aac --wraps='youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail aac' --description 'alias yta-aac youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail aac'
  youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail aac $argv; 
end
