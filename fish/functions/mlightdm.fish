# Defined via `source`
function mlightdm --wraps='sudo micro /etc/lghtdm/lightdm.conf' --description 'alias mlightdm sudo micro /etc/lghtdm/lightdm.conf'
  sudo micro /etc/lghtdm/lightdm.conf $argv; 
end
