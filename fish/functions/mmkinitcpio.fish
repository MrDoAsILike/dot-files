# Defined via `source`
function mmkinitcpio --wraps='sudo micro /etc/mkinitcpio.conf' --description 'alias mmkinitcpio sudo micro /etc/mkinitcpio.conf'
  sudo micro /etc/mkinitcpio.conf $argv; 
end
