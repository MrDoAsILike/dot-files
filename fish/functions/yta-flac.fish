# Defined via `source`
function yta-flac --wraps='youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail flac' --description 'alias yta-flac youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail flac'
  youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail flac $argv; 
end
