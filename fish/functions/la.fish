# Defined via `source`
function la --wraps='exa --icons --color-scale --color=auto --group-directories-first -F -a' --description 'exa long list including all with colors'
  exa --icons --color-scale --color=auto --group-directories-first -F -a $argv; 
end
