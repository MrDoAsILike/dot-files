# Defined via `source`
function mconfgrub --wraps='sudo micro /boot/grub/grub.cfg' --description 'alias mconfgrub sudo micro /boot/grub/grub.cfg'
  sudo micro /boot/grub/grub.cfg $argv; 
end
