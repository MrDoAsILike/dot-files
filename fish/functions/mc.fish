# Defined via `source`
function mc --wraps=micro --description 'Micro text editor'
  micro $argv; 
end
