# Defined via `source`
function tobash --wraps=sudo\ chsh\ orkah\ -s\ /bin/bash\ \&\&\ echo\ \'Now\ log\ out.\' --description alias\ tobash\ sudo\ chsh\ orkah\ -s\ /bin/bash\ \&\&\ echo\ \'Now\ log\ out.\'
  sudo chsh orkah -s /bin/bash && echo 'Now log out.' $argv; 
end
