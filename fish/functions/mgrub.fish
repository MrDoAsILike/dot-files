# Defined via `source`
function mgrub --wraps='sudo micro /etc/default/grub' --description 'alias mgrub sudo micro /etc/default/grub'
  sudo micro /etc/default/grub $argv; 
end
