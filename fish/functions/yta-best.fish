# Defined via `source`
function yta-best --wraps='youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail best' --description 'alias yta-best youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail best'
  youtube-dl --extract-audio --audio-format --add-metadata --embed-thumbnail best $argv; 
end
