# Defined via `source`
function ls --wraps=exa --description 'exa list with colors'
  exa --icons --color-scale --color=auto --group-directories-first -F $argv; 
end
