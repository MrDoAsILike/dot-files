# Defined via `source`
function tozsh --wraps=sudo\ chsh\ orkah\ -s\ /bin/zsh\ \&\&\ echo\ \'Now\ log\ out.\' --description alias\ tozsh\ sudo\ chsh\ orkah\ -s\ /bin/zsh\ \&\&\ echo\ \'Now\ log\ out.\'
  sudo chsh orkah -s /bin/zsh && echo 'Now log out.' $argv; 
end
