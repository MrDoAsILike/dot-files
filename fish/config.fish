# neofetch
neofetch

# setting default editor
export ALTERNATE_EDITOR=""
export EDITOR="micro"          # $EDITOR opens in terminal
export VISUAL="micro"          # $VISUAL opens in GUI mode
